!DISCLAIMER! The content contained herein that maybe/is marked as "Copyright"
is readily available/accessible through the means of the internet (as
presented below) and has been released with the intent of being used for
educational, research, criticism and commenting purposes with no intent of
infringement with no potential market value (free usage) under the Fair Use
Act of 1976 Section 107: Fair Use in conjunction with the Creative Commons
Attribution-NonCommercial-ShareAlike 4.0 license along with the GNU GENERAL
PUBLIC LICENSE (GPL).

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), without
limitation the rights to use, copy, modify, merge, publish, distribute,
sublicense, and to permit persons to whom the Software is furnished to do so
though not deal/sell copies of the Software, subject to the following
conditions: Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer; Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or other
materials provided with the distribution; The names of the contributors may
not be used to endorse or promote products derived from this software without
specific prior written permission. The above copyright notice and this
permission notice shall be included in all copies or substantial portions of
the Software. This software is provided by the Copyright holders and
contributors "as is", without warranties nor conditions of any kind and any
express or implied warranties, including, but not limited to the warranties of
fitness for a particular purpose are disclaimed and non-infringement. In no
event shall the Copyright holders and contributors be liable for any direct,
indirect, incidental, special, exemplary, or consequential damages (including,
but not limited to, procurement of substitute goods or services; loss of use,
data, or profits; or business interruption) how ever caused and on any theory
of liability, whether in action of contract, strict liability, or tort
(including negligence or otherwise) arising in any way out of the use of this
software, even if advised of the possibility of such damage.