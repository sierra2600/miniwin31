Index of the Potion: How to / Acknowledgments / References

The CAB method did not work for me but I left it in here incase someone else
can win that fight to make a CAB file small yet extractable. The intent was
that I had found that EXTRACT.EXE and a CAB file were much smaller than ZIP
but durring a test, EXTRACT said that it could not identify the compression
method that was used on the CAB and could not extract anything.

!NOTICE! This is quite a long document for a not so long process (it was a
long process for me to find all of this though so use it wisely and hopefully
you can do better than what I have done so far...)

######################## [(How to)] make this disk... ########################

PutDaSqueezeOnEmZIP/CAB.BAT

	This WILL REQUIRE DOSBox, DIET, aPACK for DOS and UPX for DOS

CopyEm4Me(wZIP/wCAB).BAT

	I'm done downloading what I need and compressing and now I am tired...
				 FINISH HIM.

Sorry, but for the self builds I cannot script a easy ZIP/CAB, it will
require... READING... "OH NO!!!"

1. Insert a blank floppy
2. You WILL want to format the disk otherwise corruption is very likely
3. Run LZDOS71E.EXE to create the bootable COMMAND disk (If you are running
	Microsoft Windows Vista or newer, you MAY have to run this as admin.)
3+1. Copy the contents of "CPY2DSK" to, well... the floppy (or use the batch
	file to methodically copy the files)
	A. Optionally do a defragmentation the floppy, things can go faster.
	I have gone ahead, created a virtual drive, and defragmented the
	Windows files within the ZIP file. Insane, I know...
5. Use how you want, enjoy, learn, etc...

 # # # # # # # # # # # # # # # (back to) How to # # # # # # # # # # # # # # #

Finding older (smaller) yet working versions of everything called for some
Wayback Machine to be required, more like A LOT for broken/missing/replaced
web pages to obtain a copy of each individual program that I could possibly
get. 4DOS.info is one place to start while other stuff can be found within
the FreeDOS project. [ https://www.freedos.org/ ]

A LOT OF 7-Zip for, uh... ZIP... IMA, IMG, EXE2Disk, (DMG was not needed but
another project of mine did).

I used an OEM copy of Microsoft Windows 3.1 and a Workgroups download from
Vetusware (an annoying website but, eh, it worked).
[ https://winworldpc.com/product/windows-3/31 ]
[ https://web.archive.org/web/20200827174118/https://vetusware.com/output/ctycsltm/Microsoft%20Windows%20for%20Workgroups%203.11.zip ]

################################ Make it yours ###############################

If you want to build your own floppy disk from scratch, you will need the
files in the [( Acknowledgments )] section below and these tools... take note
that each file below needs to also be compressed a specific way. aPACK does
make everything wonderful looking size wise but it WILL break boot up.

ALSO, if you are copying files to the floppy With Windows copy and paste, copy
the Windows ZIP file [ MWFW3.ZIP ] to the disk LAST otherwise PKUNZIPjr will
not unzip it for some odd reason, it is something with how Windows copies it
to the floppy (I was using Microsoft Windows 7 Ultimate for this). Side note:
Windows 10 Pro does NOT support creating boot floppies anymore.

Tools needed to do it yourself...

CABARC.EXE from cabsdk.exe [ https://theether.net/download/Microsoft/Utilities/Cabsdk.exe ]

7-Zip [ https://www.7-zip.org/a/7z1900-x64.exe ]
[ https://portableapps.com/redirect/?a=7-ZipPortable&s=s&d=pa&f=7-ZipPortable_19.00.paf.exe ]

DOSBox [ https://download3.portableapps.com/portableapps/DOSBoxPortable/DOSBoxPortable_0.74.3.paf.exe?20190321 ]
optionally also the Windows Command Prompt

DIET 1.45f [ http://old-dos.ru/dl.php?id=143 -Note: Everything is in Russian ]

aPACK [ http://www.ibsensoftware.com/files/apack-1.00.zip ]
 - So far the best packer,in Windows Command Prompt use aPACKw.exe

UPX for DOS [ https://github-production-release-asset-2e65be.s3.amazonaws.com/67031040/2df84480-3d73-11ea-8c53-1c9b33d4e51e?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20200820%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200820T033419Z&X-Amz-Expires=300&X-Amz-Signature=b7b5e92b6b47d81e5df1aef1125dd650a0681a3fab84017ff98edd5b2af8a64e&X-Amz-SignedHeaders=host&actor_id=0&repo_id=67031040&response-content-disposition=attachment%3B%20filename%3Dupx-3.96-dos.zip&response-content-type=application%2Foctet-stream ]

UPX for Windows x32 [ https://github-production-release-asset-2e65be.s3.amazonaws.com/67031040/2f297180-3d73-11ea-81eb-e3192446510e?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20200820%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20200820T033431Z&X-Amz-Expires=300&X-Amz-Signature=0afd8671b29684dde7d34a829ded3fbeb9ab0bbec39d0fe31461280b4e4a1e2d&X-Amz-SignedHeaders=host&actor_id=0&repo_id=67031040&response-content-disposition=attachment%3B%20filename%3Dupx-3.96-win32.zip&response-content-type=application%2Foctet-stream ]

PKLITE 1.12 [ http://www.win3x.org/win3board/viewtopic.php?t=6991 ]
and/or PKLITE 2.01 [ https://sta.c64.org/dosprg/pklts201.zip ]
You need both because each version will not compress certain executables but
it does not compress as good as UPX and aPACK

############################ What is compressible? ###########################

 # # # # # # # # # # # # # # # # # # DIET # # # # # # # # # # # # # # # # # #
 
Dr. Teddy's 'DIET' program for files - version 1.45f copyright(c) 1992,6/25 by
Teddy Matsumoto will only compact this very specific file and nothing else

EMM386.EXE from Microsoft Windows 3.1 NOT 3.11 for Workgroups

(aPACK can also compress this smaller than DIET but then the disk WON'T BOOT)

 # # # # # # # # # # # # # # # # # # aPACK # # # # # # # # # # # # # # # # # #
 
DOSKEY.COM	INIFILE.COM	WBAT.COM	Killer.EXE	DOSLFN.COM
PKUNZIPjr.COM (it is funny that this is the only packer that will pack this)
SHARE.EXE	SHSUCDX.EXE	XMSDSK.EXE	ZENO.EXE
CTMOUSE.EXE is already compressed by developer with aPACK (where I learned
that aPACK existed)

 # # # # # # # # # # # # # # # # # #  UPX  # # # # # # # # # # # # # # # # # #
 
HIMEM.SYS	VIDE-CDD.SYS (compressing with aPACK causes sudden reboot)

 # # # # # # # # # # # # # # # # # # Nope. # # # # # # # # # # # # # # # # # #
 
COMMAND.COM	IO.SYS		SYS.COM		DREADY.COM (too small already)
ESCAPE.EXE	ShutDown.COM	TM.EXE

 = = = = = = = = = = = = Files needed in the ZIP/CAB = = = = = = = = = = = = =
 
This contains a mix of both Windows 3.1 and Windows for Workgroups 3.11.
It needs to be created with a 7-Zip Ultra ZIP or CABARC -m LZX:21 option. It
has been found that the Windows 3.1 PROGMAN is not as dependent on other files
to operate (found out the hard way when attempting to just boot Workgroups
version "Missing this, that and the other thing")

Using the Packer programs on these files then to have them compressed into the
ZIP is actually unnecessary as it does not really save any space and DOES
cause execution problems other than ONE exception...

If you want, you can get these files straight from the install files on the
floppies by using the EXPAND command included on the first install disk.

43,072	CALC.EXE

18,512	CLIPBRD.EXE

16,416	CLOCK.EXE

5,968	COMM.DRV	from Microsoft Windows 3.11 for Workgroups

89,248	COMMDLG.DLL

15,872	CONTROL.EXE

69	CONTROL.INI

169	DOSAPP.INI

545	DOSPRMPT.PIF

2,560	FMLFN.DRV	from DOSLFN.COM ZIP

220,800	GDI.EXE

7,568	KEYBOARD.DRV

75,490	KRNL386.EXE

9,936	LZEXPAND.DLL

148,560	MAIN.CPL

10,672	MOUSE.DRV

28	MOUSE.INI

16,304	MULTIPAD.EXE	it is simply just smaller than NOTEPAD

55,168	PIFEDIT.EXE

115,312	PROGMAN.EXE

110	PROGMAN.INI

5,755	PROGRAMS.GRP

2,556	REG.DAT

57,936	SERIFE.FON

40,944	SHELL.DLL	from Microsoft Windows 3.11 for Workgroups

3,440	SOUND.DRV

64,544	SSERIFE.FON

2,304	SYSTEM.DRV

675	SYSTEM.INI

264,016	USER.EXE

9,008	VER.DLL

16,384	VGA.3GR

73,200	VGA.DRV

5,360	VGAFIX.FON

5,168	VGAOEM.FON

7,280	VGASYS.FON

17,403	WIN.COM		UPX packs this smaller than aPACK, normally is 44k

407	WIN.INI

12,800	WIN87EM.DLL

544,789	WIN386.EXE	REALLY DOESN'T LIKE TO BE PACKED

146,864	WINFILE.EXE	(Smaller ones exist, where, dunno)

98	WINFILE.INI

49,248	WINOA386.MOD	(Smaller ones exist, where, dunno)

########################## Reduce seek time details ##########################

If you are as paranoid as I am, you can copy the files into the floppy in this
exact order in order to attempt to reduce seek time. If you are using Windows
to copy these files in and are having issues with PKUNZIPJr extracting, copy
the ZIP/CAB in SECOND TO LAST otherwise you can use the "CopyEm4Me(wZIP).bat".

1. MSDOS.SYS	2. CONFIG.SYS	3. HIMEM.SYS	3+1. EMM386.EXE
5. VIDE-CDD.SYS	6. IFSHLP.SYS	7. AUTOEXEC.BAT	8. KILLER.EXE	9. ZENO.EXE
10. TM.EXE	11. CTMOUSE.EXE	12. WBAT.COM	13. DOSLFN.COM
14. LFNXLAT.386	15. SHSUCDX.EXE	16. DOSKEY.COM	17. CP437UNI.TBL
18. SHARE.EXE	19. ESCAPE.EXE	20. WIN.BAT	21. XMSDSK.EXE
22. FINDRAMD.EXE	23. PKUNZJR.COM	24. MWFW3.ZIP/MMW31.CAB	25. DREADY.COM
26. INIFILE.COM	27. LEAVE.BAT	28. SHUTDOWN.COM	29. MWCONFIG Folder

##### [(Acknowledgments)] (with comments and links to where to get them) #####

[Applies to all of the editable files] They are based off of the China DOS
Union version but removed some to A LOT of unnecessary things including >NULs
So the (c) of the used programs can be shown (even if they are listed here)
and gives you an idea of the progress of what is going on, especially hence,
the DOSBox issues
[ https://archive.org/details/mwin3 ]

 # # # # # # # # # # #  Human readable Text based files  # # # # # # # # # # #

CONFIG.SYS has had 13 unnecessary lines removed, rearranged things so it was
more streamlined, not reiterating things

AUTOEXEC.BAT has had 19 unnecessary lines removed as well as >NULs and a GOTO

WIN.BAT has had 66 unnecessary lines re/moved, ECHOs, >NULs and A LOT of GOTOs
that were just thrown [/EVERYWHERE/], they are now in an organized workflow
and 17 of those lines were offloaded to LEAVE.BAT

LEAVE.BAT has been separated from WIN.BAT to allow for a user to access from
DOS without having to go into Windows (again) *HINT*

MSDOS.SYS was needed but it also is, again, also slimmed down, extra
unnecessary lines are removed

 # # # #  Executables/Programs/Applications/"Apps" as the kids say...  # # # #

COMMAND.COM, IO.SYS and SYS.COM are LZ-DOS Version 7.10 (C)Copyright Software
Products and Systems P.L.C. 1981-2002.
	(a funny warning in the HEX that says "NEVER TRY TO PACK COMMAND.COM:
	IT WON'T WORK!!") This is very true as certain packed EXE/COM/SYS/DLLs
	need to extract first. This is already much smaller than that of the
	China DOS Union Version which is about the same size as Microsofts,
	suspicious, oh that is because if you view the HEX it is just
	Microsofts COMMAND.COM
!NOTE!	This version [DOES HAVE MISSING COMMANDS] like TYPE and LOCK, I am not
	at all sure why, but hence why you may see "Unknown command or file
	name..." during Windows loading, it is trying to run
	"ECHO y|lock %RAMDRIVE%". "lock" is a no go but it has been left in
	incase you want to use a different interpreter
[ https://web.archive.org/web/20140209004812/http://dos.nm.ru/LZDOS71E.EXE ]

CTMOUSE.EXE - CuteMouse v1.9 [FreeDOS]
[ https://sourceforge.net/projects/cutemouse/files/cutemouse%20-%20stable/1.9/ctm19bin.zip/download ]

Enhanced DOSKEY.COM Ver 1.8 - edits, recalls, auto-completes commands.
Copyright 2003 Paul Houle (paulhoule.com).  All rights reserved.
[ http://paulhoule.com/doskey/doskey18.zip ]

DREADY.COM Version 2.2 (c) 1997, Horst Schaeffer
	This is used to check if a "disk is ready" on a selected drive and if
	the disk is write protected or not - Not compactable (TOO small - UPX)
[ https://www.horstmuc.de/horst.htm ]

INIFILE.COM 1.2 (c) 2003, Horst Schaeffer
	INI Parser, simply - use aPACK
[ https://www.horstmuc.de/div.htm#inifile ]

WBAT.COM ver 2.40 (c) 1999-2003, Horst Schaeffer
	This is a more fun replacement to CHOICE *HINT* I had planned on
	replacing this with a smaller version also by Horst Schaeffer but then
	I tried something in the LEAVE.BAT that made me keep it - use aPACK
[ ftp://sestar.synchro.net/file.dist.net/BFDS/wbat240.zip ]
[ ftp://sestar.synchro.net/file.dist.net/BFDS/00index.html ]
[ https://www.horstmuc.de/ui.htm ]

ESCAPE.EXE v3.6(c)1994-2001 Selcuk Ayguney / David Lindauer *
	Pressing F12 will kill any currently running program – no compress!!!
[ https://web.archive.org/web/20070821214901/http://uk.geocities.com/software_at_short_stop/dl/escape.zip ]

Killer.EXE v1.0 Copyright 1995 Vincent Penquerc'h. All Rights Reserved.
	Will detect programs that attempt to make calls that processor cannot
	handle and close them to prevent crashes, this program is also very
	picky about the order of drivers and memory managers are loaded into
	memory otherwise it will cause the system to lockup and no amount of
	Ctrl+Alt+Del will work to get out of that mess - use aPACK
[ ftp://sestar.synchro.net/file.dist.net/BFDS/killr100.zip ]

CP437UNI.TBL, LFNXLAT.386, FMLFN.DRV (found in the ZIP) and
DOSLFN.COM 0.32o: (386+)  ++ Freeware for MS-DOS 7.10 ++
Program that supports long filenames in pure DOS.
USE THIS PROGRAM AT YOUR OWN RISK, DATA LOSS MAY BE POSSIBLE
	A CHOICE is available during startup of the floppy (AUTOEXEC) - aPACK
[ https://web.archive.org/web/20051030070138/http://www-user.tu-chemnitz.de/~heha/hs_freeware/doslfn.zip ]
[ Info found: https://sites.google.com/site/vdeeditor/Home/vde-files/doslfn-manual ]

PKUNZIPjr(TM) FAST! Mini Extract Utility Version 2.50 03-01-1999 Copr.
1989-1999 PKWARE Inc. All Rights Reserved.
	PKUNZIPjr.COM WILL NOT EXTRACT TO THE SAME DISK THAT THE ZIP FILE IS
	ON. Only from one disk to another disk (I figured out the hard way
	while trying to verify that it would unzip a 7-Zip Ultra ZIPed file)
[ http://rgstation.cafe24.com/?module=file&act=procFileDownload&file_srl=49459&sid=64bf39f4ea49f516c49a1531bf13e099&module_srl=78 ]

SHSUCDX.EXE Version 2.1a (c)John H. McCoy, October 2000, Sam Houston State
University, Jason Hood, October 2003. http://shsucdx.adoxa.cjb.net/
	CD Drive driver loader - use aPACK
[ https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/shsucdx/shcdx21a.zip ]

SHARE.EXE v7.00 (Revision 4.11.1492) Copyright (c) 1989-2003 Datalight, Inc.
[ https://web.archive.org/web/20200824063556/https://vetusware.com/output/euxbiikp/ROM_DOS_7.10_Rev_4.11.1305.zip ]

ShutDown.COM v1.2 - www.blacklight.wxs.org
	allows you to "shutdown" and restart the computer *HINT* - no compress
[ http://dosprograms.info.tt/download/shut12.zip ]

TM.EXE - Text Mode, Inkutils v1.53, Copyright 1995-1999 by Mark Incley.
	It is like MODE but cooler - Not compressible
[ https://web.archive.org/web/20020803070740/http://www.inkland.org/inkutils/ink153.exe ]
[ Need to use DOSBox or a VM to "install" (extract) first ]

XMSDSK : adjustable XMS RAMdisk (v1.9I - 08/98) Copyright (C) Franck UBERTO
38000 Grenoble, FRANCE
[ http://www.geocities.ws/ptadrous/Ramdsk.zip ]

ZENO v1.74 - Extra fast video output accelerator by Elviro Mirko
(beast2@freemail.it) May 22, 2000
[ https://web.archive.org/web/20010717223930/http://members.aol.com/files4u/ZENO174.ZIP ]
[ Info found: https://contents.driverguide.com/content.php?id=92477&path=SOFTWARE.TXT ]

 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

[ HUNT - I believe that I just used the one from the China Union DOS disk... ]

Reading the HEX of FINDRAMD.EXE shows "Portions Copyright (c) 1983,92 Borland"
	It finds the RAMDisk and report "RAMDISK=[Drive Letter]"
C:\WINDOWS\COMMAND\EBD of Windows 98?

SHARE.COM is completely different from SHARE.EXE

SHARE Emulator v1.1 is all it says

##############################################################################

Other files...

HIMEM.SYS is from Windows 3.1 and compressed with UPX not aPACK, it was large.
Notice that if HIMEM is loaded there are some programs that will hate it and
some will thankfully detect it and report it cannot function with it being
loaded, found this out the hard way while trying to update a BIOS within
FreeDOS 1.2

IFSHLP.SYS is a special one, VERY small, I'm not sure where they got it from.
It is normally found in Windows 3.1 and 3.11

VIDE-CDD.SYS is by Acer and compressed with UPX - DO NOT COMPRESS WITH aPACK
[ http://vogonsdrivers.com/files/downloader.php?fileid=1456 ]
Incase you are having new hardware issues?
[ https://sites.google.com/site/pcdosretro/vide-cdd/VIDE-CDD.ZIP?attredirects=0 ]

################################### Theirs ###################################

have an IO.SYS in theirs that was from Microsoft Windows 98

have WRITEXT and it just did color changes to text

does not have MSDOS.SYS

... I don't know what the empty MWIN3 with no extension is for... (there is no
need to unnecessarily fill the File Allocation Table with this empty stuff...)

################################# Odd things #################################

MINIKBD.DLL - this one is not even needed to function from the China DOS Union

The boot screen says Windows 3.11 for Workgroups but the About says just 3.1

############################### [(References)] ###############################

http://reboot.pro/topic/5497-ms-dos-71/

https://dosprograms.info.tt/program.htm

https://www.robvanderwoude.com/batchtools.php

https://www.4dos.info/

https://winworldpc.com/product/ms-dos/622

http://www.mdgx.com/drv.htm

http://reimagery.com/fsfd/menu.htm

https://www.bttr-software.de/freesoft/os.htm

https://www.terabyteunlimited.com/downloads-free-software.htm

http://www.321download.com/LastFreeware/page3.html

ftp://ftp.lip6.fr/pub/pc/garbo/pc/tsr/

https://www.allbootdisks.com/disk_contents/me.html

https://vetusware.com/

a worthy read - not a TL;DR: https://jdebp.uk/FGA/dos-windows-boot-process.html

Debatable: Internet Archive: Wayback Machine - https://archive.org/web/web.php

https://www.hiren.info/