Index: Preface / How to / Prologue / Epilogue (*HINT* "Downloads" on left menu)

################################ [(Preface)] #################################

Read the LICENSE.TXT file first! I'm not going to be held liable if you didn't

Microsoft Windows 3.11 was officially no longer available for OEM distribution
and offical support on Saturday, November 1, 2008.

------------------------------------------------------------------------------

!ALSO! If your Anti-Virus guard is having a FREAK OUT, this is typical!
EXE/COM/SYS(/DLL) files that have their size reduced with a [packer] (DIET,
PKLITE, UPX, and Apack) are /very/ well known to /TRIGGER/ certain Anti-virus
guards (seriously, look it up). A great majority of the programs needed to
get Windows to boot and fit onto this disk needed to be [packed]. "Let's
(NOT) get dangerous." I am [NOT] going to tell you to disable your
anti-virus. "Relax, don't do that!"

######################## [(How to)] make this disk... ########################

I have provided options: "How tough are you?" - Wolfenstein 3D (Video game'92)

(All the odd files [EXE, IMG] are obtainable from Downloads on the left menu)

JustMakeMeAFloppyV#.exe	-	"Can I play, Daddy?"
	I dunno much but I want to see what it is about / I want it I now!
	(or lazy? shrugs - oh stop your screaming fit...)
1. Insert a blank floppy
2. You WILL want to format the disk otherwise corruption is very likely
3. Run the JustMakeMeAFloppy.exe (If you are running Microsoft Windows Vista
	or newer, you MAY have to run this as an Administrator.)
3+1. Use how you want, enjoy, learn, etc...

IMGv#.IMG / IMZ	-	"Don't hurt me."
	I want write it my way, extract it, read the hex

 # # # # # "Bring 'em on!" Just, trying to make it easy for ya... # # # # # #

Ah, you want to go down the rabbit hole, eh? GOTO the DIY folder for the Potion

 = = = = = = = = = = = = = = = = = Final Boss = = = = = = = = = = = = = = = =

DoesItWorkOnaUSB.exe	-	"I am Death incarnate!" (was an option but
	sorry, I don't feel like paying for WinImage)
	- I like to break stuff... now! (use UNetbootin and the IMG file If
	you want to try running this on a USB thumb stick flash drive. I have
	tried to use Rufus but it just caused it to reboot (endlessly, heh heh!)
	
 = = = = = = = = = = = = = = = = = = EMM386 = = = = = = = = = = = = = = = = =

On certain computers, using the EMM386 will cause KILLER to trigger on A LOT,
Examples are:
	ASUS TUSI-M SiS bus, Intel Pentium !!!-S (3-Server) 1GB RAM
	Acer Aspire TimelineX i5-2430M with nVidia 540M, 8GB RAM (this laptop
		will also lock up the whole system regardless when ever
		quiting Windows, it also wont respond to shutdown in SHUTDOWN

################################# RAMDisk... #################################

The WIN.BAT is currently set to use 4MB (aim for 5MB or more of total system
RAM? No, I do not currently have an older PC to try this on; it was all
originally tested on a Intel Pentium 4 with 2GB of RAM)

*HINT*	Use this disk on a PC that does not have an ATX power supply and try
the Shutdown option for an attempt for a hit of nostalgia (from LEAVE.BAT)

########## [(Prologue)] - "Didn't China DOS Union already do this?" ##########

Starting from the beginning, with this /wonderful/ 2020 COVID-19 stuff going
around... "Yay... social distancing... that's a thing now... :( " I had time
(and I stayed up late a lot, too) to keep myself amused (even though I am
also working at the same time as well... thank goodness to everyone that is
able to, too! e.g. The Essential Workers. Please do us a favor, stay home,
stop clogging up the highways/freeways, it is not funny, it is extremely
frustrating to have someone taking a leisurely drive and going extremely slow
in the fast lane. No, leave early is not an option for me and a lot of other
people, too. Timing can be enforced by employers like mine.)

ANYWAYS. I saw a video by Michael MJD on YouTube and could not help but to
notice at about the 6-minute mark that the floppy was absolutely [CRAMMED
FULL]. I could not help it, I thought to myself "I can surely do better than
that!" and make it so someone could fit "a chapter of a story to read or
write or put more programs on should one need to" without having to disk swap
even if Windows is already loaded into RAM. (And, I did.)

One other thing that was going through my head was "How much can I trust
their version not to do something <insert your blown out of proportion
thoughts here>?" Well, without having to put it on a disk, I extracted the
disk writer and looked it completely over finding a [/VERY/] great amount of
it could be found online!

This project was also to give insight into how Windows 3.1 is loaded off of a
floppy. (Maybe even try to get it to fit on a 5.25 inch floppy, but I don't
think I have achieved that...)

Another thing, the China DOS Union version does use the [MANDARIN], not
Chinese, [MANDARIN] release. The language in itself takes up more space since
each character is in Unicode-8, a format that can easily use up "1 Dword",
8 bytes or 32 bits while the Latin or Roman alphabet takes up 1 byte or
8 bits per character.

Side note: I have usually found in text files that are made with any Windows
program will insert a "Word" or 2 bytes in order to generate a "Carriag
Return Line Feed" (or an "entered" line for those who are looking at this and
thinking "Wha?"). The same thing occurs when trying to use
"ECHO *TEXT* >> FileName.Ext" or even the old EDIT.COM program. The same
occurs with MULTIPAD. EDIT.COM also places an extra 2 Bytes even if you swap
a single character in EDIT.COM. I used Notepad on Windows 7 to rewrite the
BAT, INI, CONFIG and MSDOS.SYS files from scratch. TextEdit on macOS, (we'll
just go with) kWrite in Linux, or just general "nano" will place one
character byte for a "Carriage Return" ("Entered") but (MS)-DOS does not like
it when there is not a "Word" for a "Return" line.

######################### [(Epilogue)] - Conclusion ##########################

Total Space free so far is approximately 133kB, just large enough to fit ONE
iteration of NTFS4DOS or TWO iterations of NTFSDOS to have it not work... WHAT?
I think it had to do with the matter that I was trying it on a Intel Pentium 4
with a SATA hard disk drive that has Vista.

 # # # # # # # # # # # # # "Can this run in DOSBox?" # # # # # # # # # # # # #

From my experience, no. A LOT of the programs used to get this chopper moving
will just crash DOSBox if you decide to run them without the "/?" switch,
otherwise, well, I hated remounting my "C" drive... A LOT.

The floppy is purely meant to be ran on a [i386] or newer with at least 5 MB
of system memory (RAM) (modify WIN.BAT to see how much RAMDisk it needs until
it breaks but note that it does have a "temp" folder that it "uses/needs")

Now, you CAN run Windows "installed". Mindows 98 will run off of an image
inside of DOSBox. When starting, I took my external USB floppy drive and
installed Windows 3.1 to the "C" drive in DOSBox; it ran and even played audio
once I setup the "Soundblaster 1.5". At this point, I now had a folder of
Windows 3.1 that I could copy from, copy the whole WINDOWS directory and
destroy the other to see when it called it quits then copy back the file, etc.

Your mileage may vary with more modern computers as well, my Acer Aspire
TimelineX 3830TG would lock up so much after exiting Windows 3.1 that the
three finger salute (Ctrl+Alt+Del) would not respond. Also, if you are using a
USB Floppy drive, do NOT unplug it and replug it in, it will crash if you try
to load a file from the floppy! Ctrl+Alt+Del twice from within Windows 3.1
will reboot the computer without issue, only reboot worked from LEAVE/SHUTDOWN
on this system as well.

 # # # # # # # # # # # # # # # "This. Is. Nuts." # # # # # # # # # # # # # # #

Well, my response is "I want to GOTO space." - Portal 2

As one of my former co-workers Jake B said "Just do it and don't suck." while
jabbing his pointer finger my way. "It was VERY effective for raids on WoW."

Retain nostalgia without breaking the bank, it is difficult... but doable.
 - Not a quote and not something I'd say consistently either (this was dumb
but I'm leaving it in to give you an idea of how the lockdown has been to me.)

Note: I write a lot of this stuff when I am half awake/asleep so yeah, a lot
of references to a lot of things...

(This was originally intended to be readable in DOS, hence the weird layout.)

 # # # # # # # # # # # # # # #  Just in case...  # # # # # # # # # # # # # # #

https://bitbucket.org/sierra2600/miniwin31/downloads/JustMakeMeAFloppyV3.exe

https://bbuseruploads.s3.amazonaws.com/43b87d5d-7662-4d80-9b80-842debd8f835/downloads/484a947c-8381-4b41-9f76-753b4d6aea8a/JustMakeMeAFloppyV3.exe?Signature=3Fk%2BewvEXxxzqrv7%2Bk71zilqDc4%3D&Expires=1601531281&AWSAccessKeyId=AKIA6KOSE3BNJRRFUUX6&versionId=S5gb4Gbk9RKLQU3bJgzmqd0WmvPnLS7Q&response-content-disposition=attachment%3B+filename%3D%22JustMakeMeAFloppyV3.exe%22

https://web.archive.org/web/20201001052143/https://bbuseruploads.s3.amazonaws.com/43b87d5d-7662-4d80-9b80-842debd8f835/downloads/484a947c-8381-4b41-9f76-753b4d6aea8a/JustMakeMeAFloppyV3.exe?Signature=3Fk%2BewvEXxxzqrv7%2Bk71zilqDc4%3D&Expires=1601531281&AWSAccessKeyId=AKIA6KOSE3BNJRRFUUX6&versionId=S5gb4Gbk9RKLQU3bJgzmqd0WmvPnLS7Q&response-content-disposition=attachment%3B+filename%3D%22JustMakeMeAFloppyV3.exe%22

https://bitbucket.org/sierra2600/miniwin31/downloads/JustMakeMeAFloppyV2.exe

https://bitbucket.org/sierra2600/miniwin31/downloads/JustMakeMeAFloppyV1.exe